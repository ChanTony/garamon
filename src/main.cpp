#include <iostream>
#include <cstdlib>
#include <list>
#include <c3ga/Mvec.hpp>

#include "C3GA/c3gaTools.hpp"
#include "C3GA/Geogebra_c3ga.hpp"
#include "C3GA/Entry.hpp"
#include "C3GA/RechercheTree.hpp"

#include "opencv/ObjectData.hpp"
#include "opencv/Object.hpp"
#include "opencv/Modele.hpp"

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <time.h>
#include <sys/time.h>
using namespace c3ga;

bool pressed = false;
void CallBackFunc(int event, int x, int y, int flags, void* userdata)
{
     std::vector<float> *data = (std::vector<float>*)userdata;
     if  ( event == EVENT_LBUTTONDOWN )
     {
         //std::cout << "Left button of the mouse is clicked - position (" << x << ", " << y << ")" << std::endl;
         pressed = true;
         data->clear();
         data->push_back(x);
         data->push_back(y);
         data->push_back(0);
 
     }
     else if ( event == cv::EVENT_MOUSEMOVE ) {
          	if(pressed) {
  		        data->clear();
		        data->push_back(x);
		        data->push_back(y);
		        data->push_back(0);
          	}
    }
    else if(event == cv::EVENT_LBUTTONUP) {
          	// Libération clic gauche de la souris
          	pressed = false;
    }
}
 
bool gameLaunch(
std::function<void(Object &, 
  Object &, 
  std::vector<std::shared_ptr<Object>> &, 
  std::vector<std::shared_ptr<Object>> &, 
  std::vector<std::shared_ptr<RechercheTree>> &, 
  std::vector<int>&,
  cv::Mat &)> buildMap,
  int AiVision = 4,
  int timer = 100
  ){

  enum botType{Guardian, Watcher};
  Object player;
  Object win;
  std::vector<std::shared_ptr<Object>> lstWall;
  std::vector<std::shared_ptr<Object>> lstNpc;
  std::vector<std::shared_ptr<RechercheTree>> lstTree;
  std::vector<int> typeBot; //0 Guardian / 1 Watcher 
  cv::Mat imageWall;
  buildMap(player,win,lstWall,lstNpc,lstTree, typeBot, imageWall);
  c3ga::Mvec<double> t;
  c3ga::Mvec<double> translator;

  cv::Mat image;
  imageWall.copyTo(image);

  win.draw(image);
  player.draw(image);
  for (auto npc :lstNpc){
    npc->draw(image);
   }

  bool hasMove = false; 

  cv::namedWindow( "C3GA Game", 0x00000001 ); // Create a window for display.
  std::vector<float> data; // get x, y of the mouse
  cv::imshow( "C3GA Game", image );                // Show our image inside it.
  while(true) {
    hasMove = false;

    auto key = cv::waitKey(timer);
    imageWall.copyTo(image);
    // Wait for a keystroke in the window
    //std::cout << "Wait" << std::endl;
    //std::cout << key << std::endl;

    setMouseCallback("C3GA Game", CallBackFunc, (void*)&data);
    if (data.size() != 0){
      int x = data[0];
      int y = data[1];
      x = (int)(x /(float)ObjectData::sizeCase+0.5)*ObjectData::sizeCase;
      y = (int)(y /(float)ObjectData::sizeCase+0.5)*ObjectData::sizeCase;
      cv::Point pt1 = cv::Point(x-2,y-10) ;
      cv::Point pt2 = cv::Point(x+2,y+10) ;
      cv::rectangle(image, pt1, pt2, Scalar(200,0,0), -1);//BGR

      pt1 = cv::Point(x-10,y-2) ;
      pt2 = cv::Point(x+10,y+2) ;
      cv::rectangle(image, pt1, pt2, Scalar(200,0,0), -1);//BGR
      auto eq = player.getEntry()._eq;  
      float pas = 0.5;

      float tx=0; 
      float ty=0; 
      if ((x-eq[0][0]*ObjectData::sizeCase) > 0){
        tx = pas;
      }
      else if ((x-eq[0][0]*ObjectData::sizeCase) < 0){
        tx = -pas;
      }
      if ((y-eq[0][1]*ObjectData::sizeCase) > 0){
        ty = pas;
      }
      else if ((y-eq[0][1]*ObjectData::sizeCase) < 0){
        ty = -pas;
      }
      t =c3ga::vector<double>(tx, ty,0);
      translator = 1-0.5*t*c3ga::ei<double>();
      player.applyDynamicTransformation(translator);
      hasMove = true;
    }
    if (key == 27) exit(1);
    
    // Test colosion
    if (hasMove){
      	//Wall
      	for (auto i : lstWall){

      		if (colisionObj(*player.getObj(), *i->getObj())){
	      		//std::cout << "in" << std::endl;
		      	translator = 1-0.5*(-t)*c3ga::ei<double>();
	            player.applyDynamicTransformation(translator);   
	      	}
      	}

	    // win
  		if (colisionObj(*player.getObj(), *win.getObj())){
        	std::cout << "Win"<< std::endl;
          return true;
      	}
    }

      	/*** Create path Bot ***/
      	for (uint i =0; i< lstNpc.size() ; i++){
        	std::shared_ptr<RechercheTree> ptrTree = lstTree[i];
        	// Guardian NPC
        	


          if (botType::Guardian == typeBot[i]){
              ptrTree->buildTree(AiVision);
              if (ptrTree->findObjInTree(*player.getObj())){
                ptrTree->TrakingObjInTree(*player.getObj());
                std::cout << "player find" << std::endl;
                lstNpc[i]->updateColor(200,0,0);
              }
              else {
                if (ptrTree->TrakingMode()){
                  ptrTree->resetTree();
                }
                lstNpc[i]->updateColor(100,0,0);
              }
              ptrTree->updatePos();
              lstNpc[i]->updateDynamicObj(*ptrTree->getForm());// PS le ptrTree est le meme que lstNpc  Donc modifier Tree modifie Npc 
          }
          else if (botType::Watcher == typeBot[i]){
              ptrTree->buildTree(AiVision);
              if (ptrTree->findObjInTree(*player.getObj())){
                ptrTree->TrakingObjInTree(*player.getObj());
                std::cout << "player find" << std::endl;
                lstNpc[i]->updateColor(200,100,0);
                ptrTree->updatePos();
                lstNpc[i]->updateDynamicObj(*ptrTree->getForm());// PS le ptrTree est le meme que lstNpc  Donc modifier Tree modifie Npc 
          
              }
              else {
                if (ptrTree->TrakingMode()){
                  ptrTree->resetTree();
                }
                lstNpc[i]->updateColor(100,50,0);
                lstNpc[i]->updateDynamicObj(*ptrTree->getForm());// PS le ptrTree est le meme que lstNpc  Donc modifier Tree modifie Npc 
          
              }

          }
      	}
      	/*** end bot play ***/

      	//bot
	    for (uint i =0; i< lstNpc.size() ; i++){
	      	auto npc = lstNpc[i];
	      	if (colisionObj(*player.getObj(), *npc->getObj())){
	            std::cout << "lost"<< std::endl;
              return false;
	        }
	    }

    //NPC
    win.draw(image);
    player.draw(image);
    for (auto npc :lstNpc){
      npc->draw(image);
   }
    cv::imshow( "C3GA Game", image );                // Show our image inside it.
    //std::cout << "data " <<c3ga::e2<double>() << std::endl;
  }
  return false;
}

///////////////////////////////////////////////////////////
int main(int argc, char *argv[]){
	int timer = 75;
	if (argc == 2){
		timer =std::stoi(argv[1]);
	}
    while(!gameLaunch(buildMap1,8,timer));
    while(!gameLaunch(buildMap2,8,timer));
    while(!gameLaunch(buildMap3,8,timer));
    return 0;
}
