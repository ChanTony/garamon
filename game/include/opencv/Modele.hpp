#pragma once
#include <iostream>
#include <cstdlib>
#include <list>
#include <vector>
#include <memory>
#include <c3ga/Mvec.hpp>

#include "C3GA/c3gaTools.hpp"
#include "C3GA/Geogebra_c3ga.hpp"
#include "C3GA/Entry.hpp"
#include "C3GA/RechercheTree.hpp"

#include "opencv/ObjectData.hpp"
#include "opencv/Object.hpp"
void buildMap1(Object &player, 
	Object &win, 
	std::vector<std::shared_ptr<Object>> &lstWall,
	std::vector<std::shared_ptr<Object>> &lstNpc,
  	std::vector<std::shared_ptr<RechercheTree>> &lstTree,
  	std::vector<int> &typeBot,
	cv::Mat &imageWall);

void buildMap2(Object &player, 
	Object &win, 
	std::vector<std::shared_ptr<Object>> &lstWall,
	std::vector<std::shared_ptr<Object>> &lstNpc,
  	std::vector<std::shared_ptr<RechercheTree>> &lstTree,
  	std::vector<int> &typeBot,
	cv::Mat &imageWall);

void buildMap3(Object &player, 
	Object &win, 
	std::vector<std::shared_ptr<Object>> &lstWall,
	std::vector<std::shared_ptr<Object>> &lstNpc,
  	std::vector<std::shared_ptr<RechercheTree>> &lstTree,
  	std::vector<int> &typeBot,
	cv::Mat &imageWall);
std::vector<c3ga::Mvec<double>> GenerateWall1();