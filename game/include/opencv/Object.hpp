#pragma once
#include <iostream>
#include <list>
#include <functional>
#include <memory>

#include <c3ga/Mvec.hpp>

#include "C3GA/c3gaTools.hpp"
#include "C3GA/Geogebra_c3ga.hpp"
#include "C3GA/Entry.hpp"

#include "opencv/ObjectData.hpp"

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;

class Object
{
public:
    Object(c3ga::Mvec<double> &obj, const std::function<void(Point, Mat)> &draw, int r=100 , int g=100, int b=100 );
	Object():_hitBox(std::shared_ptr<ObjectData>()),_obj(std::shared_ptr<c3ga::Mvec<double>>()),_r(100),_g(100),_b(100){};
    ~Object(){};

    Object* ObjectDynamique(c3ga::Mvec<double> &obj, int r , int g, int b );
    Object* ObjectDynamique(c3ga::Mvec<double> &obj);
    Object* ObjectStatic(c3ga::Mvec<double> &obj, int r , int g, int b );
    Object* ObjectStatic(c3ga::Mvec<double> &obj);
    void updateDynamicObj(c3ga::Mvec<double> &obj);
    void changeDraw(const std::function<void(Point, Mat)> &draw);
	void applyDynamicTransformation(c3ga::Mvec<double> &op );

    void draw(cv::Mat img){_hitBox->draw(img);}
    Entry getEntry(){return _hitBox->getEntry();}
    std::shared_ptr<c3ga::Mvec<double>> getObj(){return _obj;}
    void updateColor(int r, int g, int b);
    void addColor(int r, int g, int b){_r+=r;_g+=g;_b+=b;}
private:
    std::shared_ptr<ObjectData>_hitBox;
    std::shared_ptr<c3ga::Mvec<double>>_obj;
    int _r;
    int _g;
    int _b;
};
