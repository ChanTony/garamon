#pragma once
#include <iostream>
#include <list>
#include <functional>
#include <C3GA/Entry.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;

class ObjectData
{
public:
    ObjectData(const Entry &hitBox, const std::function<void(Point, Mat)> &draw):_hitBox(hitBox), _draw(draw){};
    ~ObjectData(){};
    void display() const;
    void draw(Mat);
    static std::vector<ObjectData> createObjFromLst(std::list<Entry> entries, const std::function<void(Point, Mat)> &draw);
    static std::vector<ObjectData> createWallFromLst(std::list<Entry> entries);
    constexpr static int sizeCase = 50;
    Entry getEntry(){return _hitBox;}

private:
    Entry _hitBox;
    std::function<void(Point, Mat)> _draw;

};

std::function<void(Point, Mat)> createDrawCircleFunction(float radius, int r = 100, int g = 100, int b = 100);
std::function<void(Point, Mat)> createDrawRectangleFunction(float x,float y, int r = 100, int g = 100, int b = 100);