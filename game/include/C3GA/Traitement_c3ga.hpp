#pragma once

#include <list>
#include <vector>
#include <iostream>
#include <string>
#include <algorithm> 
#include <cstdlib>
#include <limits>

#include <c3ga/Mvec.hpp>

#include "c3gaTools.hpp"
#include "Entry.hpp"
#include "Directory.hpp"
#include "Geogebra_c3ga.hpp"


class Traitement_c3ga : public Viewer_c3ga 
{
    public :
        using Viewer_c3ga::Viewer_c3ga;

        template<typename T>
        static Entry generateEntry(c3ga::Mvec<T> obj){
            Viewer_c3ga viewer;
            viewer.push(obj, "");
            auto lst = viewer.getEntries();  
            return lst.front();
        }
};