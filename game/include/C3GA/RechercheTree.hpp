#pragma once
#include <iostream>
#include <list>
#include <functional>
#include <memory>
#include <stdexcept>
#include <stdlib.h>
#include <time.h>      
#include <stdio.h>
#include <omp.h>
#include <sys/time.h>
#include <vector>


#include "C3GA/c3gaTools.hpp"
#include "C3GA/Geogebra_c3ga.hpp"
#include "C3GA/Entry.hpp"
#include "C3GA/Traitement_c3ga.hpp"

bool colisionObj(const c3ga::Mvec<double> &o1,const c3ga::Mvec<double> &o2);

class Node
{
    public :
    std::shared_ptr<c3ga::Mvec<double>> _form; 
    std::vector<std::shared_ptr<Node>> _childs; 
    int _comeFrom; 
    
    public :
    Node(std::shared_ptr<c3ga::Mvec<double>> form,int comeFrom = -1):_form(form),_childs(std::vector<std::shared_ptr<Node>>()),_comeFrom(comeFrom){}
    ~Node(){};

    // childs tools
    void reset(){_childs.clear();}
    bool hasChilds(){return _childs.size()!=0;}
    void addChild(std::shared_ptr<Node> ptr, int comeFrom = -1){
        _childs.push_back(ptr);
        _comeFrom = comeFrom;
    }
    std::shared_ptr<Node> addChild(std::shared_ptr<c3ga::Mvec<double>> ptr, int comeFrom = -1);
    std::shared_ptr<Node> firstChild();
    std::shared_ptr<Node> randChild();

    // build tools
    void buildNode(std::vector<c3ga::Mvec<double>> walls,bool traking, int depth, std::vector<std::vector<int>> &exist, int pos , int previousMove = -1);
    bool buildTrakingNode(const c3ga::Mvec<double> &obj, std::vector<c3ga::Mvec<double>> walls,bool traking, int depth, int previousMove = -1);

    //Trakings tools
    bool findObjInNode(const c3ga::Mvec<double> &obj);
    bool TrakingObjInNode(const c3ga::Mvec<double> &obj);
    void KeepShortNode();
    
    //utils
    int getDepth();
    int getNumNode();
    void lstForm(std::vector<c3ga::Mvec<double>> &vecForm);
    
    //viewer
    void viewGeogebra(Viewer_c3ga &viewer);

      
};

 

class RechercheTree
{
    public :
    RechercheTree(std::vector<c3ga::Mvec<double>> walls, std::shared_ptr<c3ga::Mvec<double>> form):_walls(walls),_tree(std::unique_ptr<Node>(new Node(form))), _tracking(false) {}
    ~RechercheTree(){};

    //childs
    void resetTree(){_tree->reset(); _tracking = false;}
    bool emptyTree(){return !_tree->hasChilds();}

    // build tools
    // depth -1 c'est la racine
    void buildTree(int depth = 4);
    bool buildTrakingTree(const c3ga::Mvec<double> &obj, int depth = 4);
    
    //Trakings tools
    bool findObjInTree(const c3ga::Mvec<double> &obj){return _tree->findObjInNode(obj);}
    //Destructif de l'arbre
    bool TrakingObjInTree(const c3ga::Mvec<double> &obj,bool keepOneNode = false);

    //UpdatePos
    void updatePos();

    //utils
    std::vector<c3ga::Mvec<double>> lstForm();
    std::shared_ptr<c3ga::Mvec<double>> getForm(){return _tree->_form;}
    int getDepth(){return _tree->getDepth();}
    bool TrakingMode(){return _tracking;}
    //viewer
    void viewGeogebra();
    
    private :
        std::vector<c3ga::Mvec<double>> _walls; 
    	std::shared_ptr<Node> _tree;
        bool _tracking;
};
