#include "opencv/ObjectData.hpp"

void ObjectData::draw(Mat image){

  Point center(_hitBox._eq[0][0]*ObjectData::sizeCase,_hitBox._eq[0][1]*ObjectData::sizeCase);
  _draw(center, image);
}

std::vector<ObjectData> ObjectData::createObjFromLst(std::list<Entry> entries, const std::function<void(Point, Mat)> &draw){
	std::vector<ObjectData> lst;
	for (auto i : entries){
		lst.push_back(ObjectData(i,draw));
	}
	return lst;
}

//circle -> point / radius / vector
std::vector<ObjectData> ObjectData::createWallFromLst(std::list<Entry> entries){
	std::vector<ObjectData> lst;
	for (auto i : entries){
		std::function<void(Point, Mat)> draw = 
		[=](Point center, Mat img){
			auto rad = i._eq[1];
			auto vec = i._eq[2];
  		    //std::cout <<center<<std::endl;
  		    //std::cout <<i._equation<<std::endl;
  		    //std::cout <<Point(center.x-((rad[0]*vec[0]*ObjectData::sizeCase))-1,center.y-((rad[0]*vec[1]*ObjectData::sizeCase))-1)<<std::endl;
			vec[0] = 1- std::abs(vec[0]);
			vec[1] = 1- std::abs(vec[1]);
  		    Point pt1 = Point(center.x-(std::abs(rad[0]*vec[0]*ObjectData::sizeCase))-1,center.y-(std::abs(rad[0]*vec[1]*ObjectData::sizeCase))-1);
  		    Point pt2 = Point(center.x+(std::abs(rad[0]*vec[0]*ObjectData::sizeCase))+1,center.y+(std::abs(rad[0]*vec[1]*ObjectData::sizeCase))+1);
			rectangle(img, pt1, pt2, Scalar(100,100,100), -1);
		};
		lst.push_back(ObjectData(i,draw));
	}
	return lst;
}

std::function<void(Point, Mat)> createDrawCircleFunction(float radius, int r, int g, int b){
	return [=] (Point center, Mat img){
		//std::cout<< center<< " "<< radius<<std::endl;
		circle(img, center, radius*ObjectData::sizeCase, Scalar(b,g,r), -1);
	};
}

std::function<void(Point, Mat)> createDrawRectangleFunction(float x,float y, int r, int g, int b){
	return [=] (Point center, Mat img){
		Point pt1 = Point(center.x-(x/2),center.y-(y/2)) ;
		Point pt2 = Point(center.x+(x/2),center.y+(y/2)) ;
		line(img, pt1, pt2, Scalar(b,g,r), 1);
	};
}