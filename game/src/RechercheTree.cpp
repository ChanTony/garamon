#include "C3GA/RechercheTree.hpp"
bool colisionObj(const c3ga::Mvec<double> &o1,const c3ga::Mvec<double> &o2){

    Entry e1 = Traitement_c3ga::generateEntry(o1);
    Entry e2 = Traitement_c3ga::generateEntry(o2);

    auto  tmpO1 = o1;
    auto  tmpO2 = o2;
    if (e1._objectName == "circle" && e2._objectName == "sphere"){
        auto tmp = e1;
        e1 = e2;
        e2 = tmp;
        tmpO1 = o2;
        tmpO2 = o1;
    }
    if (e1._objectName == "sphere" && e2._objectName == "circle"){
     
        c3ga::Mvec<double> plan = (o2) ^c3ga::ei<double>();
        c3ga::Mvec<double> colision = (!plan)^(!o1);
        double rad = ((!colision)|(!colision));

        if (rad >= 0){
        	// dualize
            tmpO1 = tmpO1.dual();
			// extract parameters
            tmpO1 /= tmpO1[c3ga::E0]; // back to correct scale
            double squareRadius = tmpO1 | tmpO1;
            double rayon = sqrt(squareRadius);
            c3ga::Mvec<double> center = tmpO1;
            center /= center[c3ga::E0];
            center[c3ga::E0] = 1;
            center[c3ga::Ei] = 0.5 * (center[c3ga::E1]*center[c3ga::E1] + center[c3ga::E2]*center[c3ga::E2] + center[c3ga::E3]*center[c3ga::E3]);
		    auto t =c3ga::vector<double>(rayon, 0, 0);
		    auto translator = 1-0.5*t*c3ga::ei<double>();
            auto centerPlayer = translator*center/translator;
            

            double radius;
            c3ga::Mvec<double> c, normalWall;
            extractDualCircle(tmpO2.dual(), radius,  c, normalWall);

            auto d = centerPlayer ^ normalWall ^ c3ga::ei<double>();
            colision = (!d)|(o2);
            rad = (colision|colision);
            if (rad < 0){
              //std::cout << "in"<< std::endl;
              return true;
            }
		    t =c3ga::vector<double>(-rayon, 0, 0);
		    translator = 1-0.5*t*c3ga::ei<double>();
            centerPlayer = translator*center/translator;

            d = centerPlayer ^ normalWall ^ c3ga::ei<double>();
            colision = (!d)|(o2);
            rad = (colision|colision);
            if (rad < 0){
              //std::cout << "in"<< std::endl;
              return true;
            }

		    t =c3ga::vector<double>(0, rayon, 0);
		    translator = 1-0.5*t*c3ga::ei<double>();
            centerPlayer = translator*center/translator;

            d = centerPlayer ^ normalWall ^ c3ga::ei<double>();
            colision = (!d)|(o2);
            rad = (colision|colision);
            if (rad < 0){
              //std::cout << "in"<< std::endl;
              return true;
            }

		    t =c3ga::vector<double>(0, -rayon, 0);
		    translator = 1-0.5*t*c3ga::ei<double>();
            centerPlayer = translator*center/translator;

            d = centerPlayer ^ normalWall ^ c3ga::ei<double>();
            colision = (!d)|(o2);
            rad = (colision|colision);
            if (rad < 0){
              //std::cout << "in"<< std::endl;
              return true;
            }
        }
    }
    else {
      c3ga::Mvec<double> colision = (!o1)^(!o2);
      double rad = ((!colision)|(!colision));

      return (rad >= 0);
    }
    return false;
}



/********** Node **********/


std::shared_ptr<Node> Node::addChild(std::shared_ptr<c3ga::Mvec<double>> ptr, int comeFrom){
    std::shared_ptr<Node> ptrNode(new Node(ptr));
    addChild(ptrNode, comeFrom);
    return ptrNode;
}

std::shared_ptr<Node> Node::firstChild(){
    if (!hasChilds()){
        throw std::runtime_error("has no child, use buildTree before updating pos");
    }
    return (_childs[0]);
}

std::shared_ptr<Node> Node::randChild(){
    srand (time(NULL));
    if (!hasChilds()){
        throw std::runtime_error("has no child, use buildTree before updating pos");
    }
    int r = rand() % _childs.size();
    return (_childs[r]);
}


bool Node::buildTrakingNode(const c3ga::Mvec<double> &obj, std::vector<c3ga::Mvec<double>> walls,bool traking, int depth, int previousMove)
{
    if (depth < 0){
        return false;
    }

    if (hasChilds()){
        for (auto i : _childs){
            if(i->buildTrakingNode(obj, walls, traking, depth-1)){
                return true;
            }
        } 
        return false;      
    }

    enum pos {Up,Down,Right,Left};
    auto racinePtr = _form;
    auto h =c3ga::vector<double>(0, 0.5, 0);
    auto b =c3ga::vector<double>(0, -0.5, 0);
    auto d =c3ga::vector<double>(0.5, 0, 0);
    auto g =c3ga::vector<double>(-0.5, 0, 0);
    std::shared_ptr<c3ga::Mvec<double>> tmp1 = std::shared_ptr<c3ga::Mvec<double>>(new c3ga::Mvec<double>(*racinePtr));
    auto translator = 1-0.5*h*c3ga::ei<double>();
    *tmp1 = translator*(*tmp1)/translator;

    std::shared_ptr<c3ga::Mvec<double>> tmp2 =  std::shared_ptr<c3ga::Mvec<double>>(new c3ga::Mvec<double>(*racinePtr));
    translator = 1-0.5*b*c3ga::ei<double>();
    *tmp2 = translator*(*tmp2)/translator;

    std::shared_ptr<c3ga::Mvec<double>> tmp3 =  std::shared_ptr<c3ga::Mvec<double>>(new c3ga::Mvec<double>(*racinePtr));
    translator = 1-0.5*d*c3ga::ei<double>();
    *tmp3 = translator*(*tmp3)/translator;

    std::shared_ptr<c3ga::Mvec<double>> tmp4 =  std::shared_ptr<c3ga::Mvec<double>>(new c3ga::Mvec<double>(*racinePtr));
    translator = 1-0.5*g*c3ga::ei<double>();
    *tmp4 = translator*(*tmp4)/translator;

    bool colis1 = (previousMove == Down);
    bool colis2 = (previousMove == Up);
    bool colis3 = (previousMove == Left);
    bool colis4 = (previousMove == Right);

    for (auto wall : walls){
        if (!colis1)
            colis1 = colisionObj(*tmp1 ,wall);

        if (!colis2)
            colis2 = colisionObj(*tmp2 ,wall);
        
        if (!colis3)
            colis3 = colisionObj(*tmp3 ,wall);
        
        if (!colis4)    
            colis4 = colisionObj(*tmp4 ,wall);
    }


    if (!colis1){
        auto ptrNode = this->addChild(tmp1);
        if (depth >0)
            if(ptrNode->buildTrakingNode(obj, walls, traking, depth-1, Up)){
                return true;
            }            
    }
    if (!colis2){
        auto ptrNode = this->addChild(tmp2);
        if (depth >0)
            if(ptrNode->buildTrakingNode(obj, walls, traking, depth-1, Down)){
                return true;
            }            
    }
    if (!colis3){
        auto ptrNode = this->addChild(tmp3);
        if (depth >0)
            if(ptrNode->buildTrakingNode(obj, walls, traking, depth-1, Right)){
                return true;
            }            
    }
    if (!colis4){
        auto ptrNode = this->addChild(tmp4);
        if (depth >0)
            if(ptrNode->buildTrakingNode(obj, walls, traking, depth-1, Left)){
                return true;
            }            
    }
    return false;
}   


bool Node::findObjInNode(const c3ga::Mvec<double> &obj){
    if (colisionObj(*_form, obj)){
        return true;
    }
    for (auto node : _childs){
        if (node->findObjInNode(obj)){
            return true;
        }
    }
    return false;
}

bool Node::TrakingObjInNode(const c3ga::Mvec<double> &obj){
    if (colisionObj(*_form, obj)){
        this->reset();
        return true;
    }
    bool traking = false;
    std::vector<std::shared_ptr<Node>> lstTmp;
    for (auto node : _childs){
        if (node->TrakingObjInNode(obj)){
            traking = true;
            lstTmp.push_back(node);
        }
    }
    this->reset();
    this->_childs = lstTmp;
    return traking;
}

int Node::getDepth(){
    if (!hasChilds()){
        return 0;
    }
    return _childs[0]->getDepth()+1;// version speed
}

int Node::getNumNode(){
    if (!hasChilds()){
        return 1;
    }
    int sum = 1;
    for (auto i : _childs){
        sum += i->getNumNode();// version speed
    }
    return sum;
}

void Node::viewGeogebra(Viewer_c3ga &viewer){
    viewer.push(*_form,"",100,100,100);
    for (auto node : _childs){
        node->viewGeogebra(viewer);
    }
}

void Node::lstForm(std::vector<c3ga::Mvec<double>> &vecForm){
    vecForm.push_back(*_form);
    for (auto node : _childs){
        node->lstForm(vecForm);
    }
}
void Node::KeepShortNode(){
    if (!hasChilds()){
        return;
    }
    std::shared_ptr<Node> tmp = _childs[0];
    int d = _childs[0]->getDepth();
    for (auto node : _childs){
        int dpth = node->getDepth();
        if (dpth < d){
            d = dpth;
            tmp = node;
        }
    }
    this->reset();
    addChild(tmp);
}

/********** Tree **********/

void RechercheTree::buildTree(int depth){
    /*if (!emptyTree()){
        //std::cout << "warning : not an empty tree" << std::endl;
        return;
    }*/
    std::vector<std::vector<int>> existance(depth);
    for (int i =0; i< depth; i++){
        existance.push_back(std::vector<int>());
    }
    _tree->buildNode(_walls, _tracking, depth, existance, 0);

    //std::cout << "D " << _tree->getDepth() << std::endl;
    //td::cout << "H " << _tree->getNumNode() << std::endl;
    //std::cout <<Traitement_c3ga::generateEntry(*_tree->_form)._equation << std::endl;
}

bool RechercheTree::buildTrakingTree(const c3ga::Mvec<double> &obj, int depth){
    /*if (!emptyTree()){
        std::cout << "warning : not an empty tree" << std::endl;
    }*/
    _tree->buildTrakingNode(obj, _walls, _tracking, depth);
    //std::cout << "D " << _tree->getDepth() << std::endl;
    //std::cout << "H " << _tree->getNumNode() << std::endl;
    //std::cout <<Traitement_c3ga::generateEntry(*_tree->_form)._equation << std::endl;

    return false;
}
//Destructif de l'arbre
bool RechercheTree::TrakingObjInTree(const c3ga::Mvec<double> &obj,bool keepOneNode){
    
    std::vector<std::shared_ptr<Node>> childs = _tree->_childs;
    bool hasC = _tree->hasChilds();
        
    _tracking = _tree->TrakingObjInNode(obj);    
    _tree->KeepShortNode(); 
    if (keepOneNode && hasC){
        _tree->_childs = childs;
    }

    return _tracking;
}
void RechercheTree::updatePos(){
    if(_tree->hasChilds()){
         _tree = ((_tree->randChild()));
    }
}

void RechercheTree::viewGeogebra(){
    Viewer_c3ga viewer;
    _tree->viewGeogebra(viewer);
    viewer.render("Tree.html");
}

std::vector<c3ga::Mvec<double>> RechercheTree::lstForm(){
    std::vector<c3ga::Mvec<double>> lstVec;
    _tree->lstForm(lstVec);
    return lstVec;
}




/********************************* Fonction optimiser *************************************/

    void Node::buildNode(std::vector<c3ga::Mvec<double>> walls,bool traking, int depth, std::vector<std::vector<int>> &exist, int position , int previousMove)
{
    if (depth < 0){
        return;
    }
    if (hasChilds()){
        for (auto i : _childs){
            i->buildNode(walls, traking, depth-1,exist, position+_comeFrom+1, _comeFrom);
        } 
        return;
    }
    exist[depth].push_back(0);
    enum pos {Up,Down,Right,Left};
    bool lstB[4];
    std::shared_ptr<c3ga::Mvec<double>> lstPtr[4];

    omp_set_num_threads(4);
    #pragma omp parallel
    {
        int numThread = omp_get_thread_num();
        c3ga::Mvec<double> vect;

        if (numThread == 0){
            vect = c3ga::vector<double>(0, 0.5, 0);
        }
        else if (numThread == 1){
            vect = c3ga::vector<double>(0, -0.5, 0);
        }
        else if (numThread == 2){
            vect = c3ga::vector<double>(0.5, 0, 0);
        }
        else if (numThread == 3){
            vect = c3ga::vector<double>(-0.5, 0, 0);
        }

        std::shared_ptr<c3ga::Mvec<double>> tmp = std::shared_ptr<c3ga::Mvec<double>>(new c3ga::Mvec<double>(*_form));
        auto translator = 1-0.5*vect*c3ga::ei<double>();
        *tmp = translator*(*tmp)/translator;


        bool colis = false;
        if (numThread == 0){
            colis = (previousMove == Down);
        }
        else if (numThread == 1){
            colis = (previousMove == Up);
        }
        else if (numThread == 2){
            colis = (previousMove == Left);
        }
        else if (numThread == 3){
            colis = (previousMove == Right);
        }
        if(!(std::find(exist[depth].begin(), exist[depth].end(),position+numThread+1)!= exist[depth].end())){
            for (auto wall : walls){
                if (!colis)
                    colis = colisionObj(*tmp ,wall);
            }
            lstB[numThread] = colis;
            lstPtr[numThread] = tmp;
        }
        else{
            lstB[numThread] = true;
            lstPtr[numThread] = tmp;
        }

    }
    
    if (!lstB[0]){
        exist[depth].push_back(position+1+Up);
        auto ptrNode = this->addChild(lstPtr[0], Up);
        if (depth >0){
            ptrNode->buildNode(walls, traking, depth-1,exist,(position+1+Up) , Up);            
        }
    }
    if (!lstB[1]){
        exist[depth].push_back(position+1+Down);
        auto ptrNode = this->addChild(lstPtr[1], Down);
        if (depth >0){
            ptrNode->buildNode(walls, traking, depth-1,exist,(position+1+Down) , Down);            
        }
    }
    if (!lstB[2]){
        exist[depth].push_back(position+1+Right);
        auto ptrNode = this->addChild(lstPtr[2], Right);
        if (depth >0){
            ptrNode->buildNode(walls, traking, depth-1,exist,(position+1+Right) , Right);            
        }
    }
    if (!lstB[3]){
        exist[depth].push_back(position+1+Left);
        auto ptrNode = this->addChild(lstPtr[3], Left);
        if (depth >0){
            ptrNode->buildNode(walls, traking, depth-1,exist,(position+1+Left) , Left);            
        }
    }

}
