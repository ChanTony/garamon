#include "opencv/Object.hpp"

Object::Object(c3ga::Mvec<double> &obj, const std::function<void(Point, Mat)> &draw, int r, int g, int b):_r(r),_g(g),_b(b){
	Viewer_c3ga viewer;
    viewer.push(obj, "");
    auto lst = viewer.getEntries();  
    _hitBox = std::shared_ptr<ObjectData>(new ObjectData(lst.front(), draw));
    _obj = std::shared_ptr<c3ga::Mvec<double>>(&obj);
}
    
void Object::updateColor(int r, int g, int b){
    if (r < 0)
        _r = 100;
    else 
        _r = r;
    if (g < 0)
        _g = 100;
    else 
        _g = g;
    if (b < 0)
        _b = 100;
    else 
        _b = b;
}

Object* Object::ObjectDynamique(c3ga::Mvec<double> &obj, int r , int g, int b){
		Viewer_c3ga viewer;
	    viewer.push(obj, "");
	    auto lst = viewer.getEntries();  
	    updateColor(r,g,b);
	    auto draw = createDrawCircleFunction(lst.front()._eq[1][0],_r,_g,_b);
	    _hitBox = std::shared_ptr<ObjectData>(new ObjectData(lst.front(), draw));
	    _obj = std::shared_ptr<c3ga::Mvec<double>>(new c3ga::Mvec<double>(obj));
	    return this;
    }

void Object::updateDynamicObj(c3ga::Mvec<double> &obj){
	Viewer_c3ga viewer;
    viewer.push(obj, "");
    auto lst = viewer.getEntries();  
    auto draw = createDrawCircleFunction(lst.front()._eq[1][0],_r,_g,_b);
    _hitBox = std::shared_ptr<ObjectData>(new ObjectData(lst.front(), draw));
	_obj = std::shared_ptr<c3ga::Mvec<double>>(new c3ga::Mvec<double>(obj));

}

Object* Object::ObjectStatic(c3ga::Mvec<double> &obj, int r, int g, int b){
	Viewer_c3ga viewer;
    viewer.push(obj, "");
    auto lst = viewer.getEntries();
    updateColor(r,g,b);
    std::function<void(Point, Mat)> draw = 
	[=](Point center, Mat img){
		auto rad = lst.front()._eq[1];
		auto vec = lst.front()._eq[2];
		        // Rorator
        auto v = c3ga::vector<double>(vec[0],vec[1],vec[2]);
        c3ga::Mvec<double> r =c3ga::e12<double>(); //c3ga::e1<double>()*c3ga::e2<double>();
        c3ga::Mvec<double> rotator = std::cos(1.5708/2)-r*std::sin(1.5708/2);
        c3ga::Mvec<double> vecC3GA= rotator*v/rotator;
        vecC3GA.roundZero(10e-3);
        Point pt1 = Point(center.x-(std::abs(rad[0]*vecC3GA[c3ga::E1]*ObjectData::sizeCase)),center.y-(std::abs(rad[0]*vecC3GA[c3ga::E2]*ObjectData::sizeCase)));
        Point pt2 = Point(center.x+(std::abs(rad[0]*vecC3GA[c3ga::E1]*ObjectData::sizeCase)),center.y+(std::abs(rad[0]*vecC3GA[c3ga::E2]*ObjectData::sizeCase)));
        line(img, pt1, pt2, Scalar(_r,_g,_b), 3);
	};

    _hitBox = std::shared_ptr<ObjectData>(new ObjectData(lst.front(), draw));
    _obj = std::shared_ptr<c3ga::Mvec<double>>(new c3ga::Mvec<double>(obj));
    return this;
}

Object* Object::ObjectStatic(c3ga::Mvec<double> &obj){
	Viewer_c3ga viewer;
    viewer.push(obj, "");
    auto lst = viewer.getEntries();
    std::function<void(Point, Mat)> draw = 
	[=](Point center, Mat img){
		auto rad = lst.front()._eq[1];
		auto vec = lst.front()._eq[2];
        // Rorator
        auto v = c3ga::vector<double>(vec[0],vec[1],vec[2]);
        c3ga::Mvec<double> r =c3ga::e12<double>(); //c3ga::e1<double>()*c3ga::e2<double>();
        c3ga::Mvec<double> rotator = std::cos(1.5708/2)-r*std::sin(1.5708/2);
        c3ga::Mvec<double> vecC3GA= rotator*v/rotator;
        vecC3GA.roundZero(10e-3);
	    Point pt1 = Point(center.x-((rad[0]*vecC3GA[c3ga::E1]*ObjectData::sizeCase)),center.y-((rad[0]*vecC3GA[c3ga::E2]*ObjectData::sizeCase)));
	    Point pt2 = Point(center.x+((rad[0]*vecC3GA[c3ga::E1]*ObjectData::sizeCase)),center.y+((rad[0]*vecC3GA[c3ga::E2]*ObjectData::sizeCase)));
		line(img, pt1, pt2, Scalar(_r,_g,_b), 3);
	};

    _hitBox = std::shared_ptr<ObjectData>(new ObjectData(lst.front(), draw));
    _obj = std::shared_ptr<c3ga::Mvec<double>>(new c3ga::Mvec<double>(obj));
    return this;
}

void Object::changeDraw(const std::function<void(Point, Mat)> &draw){
	Viewer_c3ga viewer;
    viewer.push(*_obj, "");
    auto lst = viewer.getEntries();  
    _hitBox = std::shared_ptr<ObjectData>(new ObjectData(lst.front(), draw));
}


//circle -> point / radius / vector
//std::vector<ObjectData> ObjectData::createWallFromLst(std::list<Entry> entries);
void Object::applyDynamicTransformation(c3ga::Mvec<double> &op ){
	*_obj = op*(*_obj)/op;
	Viewer_c3ga viewer;
    viewer.push(*_obj, "");
    auto lst = viewer.getEntries();
    auto draw = createDrawCircleFunction(lst.front()._eq[1][0],_r,_g,_b);
    _hitBox = std::shared_ptr<ObjectData>(new ObjectData(lst.front(), draw));
	//ObjectDynamique(*_obj,_r,_g,_b);
}