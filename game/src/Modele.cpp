#include "opencv/Modele.hpp"

c3ga::Mvec<double> generateWall(int x1,int y1,int x2,int y2){
  int xmin = std::min(x1,x2);
  int ymin = std::min(y1,y2);
  int xmoy = std::abs(x1-x2)/2.;
  int ymoy = std::abs(y1-y2)/2.;
  return  c3ga::point<double>(x1,y1,0)
                          ^ c3ga::point<double>(x2,y2,0)
                          ^ c3ga::point<double>(xmin+xmoy,ymin+ymoy,std::sqrt(xmoy*xmoy+ymoy*ymoy));
}
std::vector<c3ga::Mvec<double>> GenerateWall3(){
  // border
  std::vector<c3ga::Mvec<double>> vec;
  c3ga::Mvec<double> wall = generateWall(0,0,0,12);
  vec.push_back(wall);
  
  wall = generateWall(0,0,20,0);
  vec.push_back(wall);

  wall = generateWall(0,0,0,14);
  vec.push_back(wall);

  wall = generateWall(0,14,20,14);
  vec.push_back(wall);

  wall = generateWall(20,0,20,14);
  vec.push_back(wall);

 // in wall
  //1
  wall = generateWall(2,10,4,12);
  vec.push_back(wall);

  //2
  wall = generateWall(4,8,12,14);
  vec.push_back(wall);
  wall = generateWall(12,14,18,8);
  vec.push_back(wall);

  //3
  wall = generateWall(2,6,8,0);
  vec.push_back(wall);
  wall = generateWall(8,0,16,6);
  vec.push_back(wall);

  //4
  wall = generateWall(6,6,8,4);
  vec.push_back(wall);
  wall = generateWall(8,4,11,6);
  vec.push_back(wall);

  //4
  wall = generateWall(12,8,12,11);
  vec.push_back(wall);

  return vec;
}

void buildMap3(Object &player, 
  Object &win, 
  std::vector<std::shared_ptr<Object>> &lstWall,
  std::vector<std::shared_ptr<Object>> &lstNpc,
  std::vector<std::shared_ptr<RechercheTree>> &lstTree,
  std::vector<int> &typeBot,
  cv::Mat &imageWall){ 

  enum botType{Guard, Watcher};
  imageWall = cv::Mat::zeros(14*ObjectData::sizeCase, 20*ObjectData::sizeCase, CV_8UC3);
  c3ga::Mvec<double> sphere = c3ga::point<double>(1,0.5,0)
                        ^ c3ga::point<double>(0.5,1,0)
                        ^ c3ga::point<double>(1.5,1,0)
                        ^ c3ga::point<double>(1,1,0.5);   

  c3ga::Mvec<double> t = c3ga::vector<double>(0,0,0);
  c3ga::Mvec<double> translator = 1-0.5*t*c3ga::ei<double>();
  auto spherePlayer = (translator*sphere/translator);
  player.ObjectDynamique(spherePlayer,0,0,200);

  //Win
  t = c3ga::vector<double>(18,0,0);
  translator = 1-0.5*t*c3ga::ei<double>();
  auto sphereWin = (translator*sphere/translator);
  win.ObjectDynamique(sphereWin,0,100,0);

  //Wall
  auto walls = GenerateWall3();
  for (int i =0; i<(int)walls.size(); i++){
    std::shared_ptr<Object> wallC3GA = std::shared_ptr<Object>(new Object());
    wallC3GA->ObjectStatic(walls[i]);
    lstWall.push_back(wallC3GA);
    wallC3GA->draw(imageWall);
  }

  //NPC
  //Positionnement bot
  typeBot.push_back(botType::Watcher);
  lstNpc.push_back(std::shared_ptr<Object> (new Object()));
  t = c3ga::vector<double>(8,6,0);
  translator = 1-0.5*t*c3ga::ei<double>();
  auto sphereNPC = (translator*sphere/translator);
  lstNpc[0]->ObjectDynamique(sphereNPC,100,50,0);
  lstTree.push_back(std::shared_ptr<RechercheTree>(new RechercheTree(walls ,lstNpc[0]->getObj())));

  typeBot.push_back(botType::Guard);
  lstNpc.push_back(std::shared_ptr<Object> (new Object()));
  t = c3ga::vector<double>(10,10,0);
  translator = 1-0.5*t*c3ga::ei<double>();
  sphereNPC = (translator*sphere/translator);
  lstNpc[1]->ObjectDynamique(sphereNPC,100,0,0);
  lstTree.push_back(std::shared_ptr<RechercheTree>(new RechercheTree(walls ,lstNpc[1]->getObj())));


}

/***** map 2 *****/
std::vector<c3ga::Mvec<double>> GenerateWall2(){
  // border
  std::vector<c3ga::Mvec<double>> vec;
  c3ga::Mvec<double> wall = generateWall(0,0,0,12);
  vec.push_back(wall);
  
  wall = generateWall(0,0,12,0);
  vec.push_back(wall);

  wall = generateWall(0,12,12,12);
  vec.push_back(wall);

  wall = generateWall(0,12,12,12);
  vec.push_back(wall);

  wall = generateWall(12,0,12,12);
  vec.push_back(wall);

 // in wall
  //1
  wall = generateWall(2,2,2,8);
  vec.push_back(wall);

  //2
  wall = generateWall(4,10,10,10);
  vec.push_back(wall);
  wall = generateWall(10,6,10,10);
  vec.push_back(wall);
  //3
  wall = generateWall(4,4,4,6);
  vec.push_back(wall);
  
  //4
  wall = generateWall(6,8,8,8);
  vec.push_back(wall);

  //5
  wall = generateWall(6,2,6,6);
  vec.push_back(wall);
  wall = generateWall(6,6,8,6);
  vec.push_back(wall);

  //6
  wall = generateWall(8,2,8,4);
  vec.push_back(wall);
  wall = generateWall(8,2,10,2);
  vec.push_back(wall);
  wall = generateWall(10,2,10,4);
  vec.push_back(wall);


  return vec;
}

void buildMap2(Object &player, 
  Object &win, 
  std::vector<std::shared_ptr<Object>> &lstWall,
  std::vector<std::shared_ptr<Object>> &lstNpc,
  std::vector<std::shared_ptr<RechercheTree>> &lstTree,
  std::vector<int> &typeBot,
  cv::Mat &imageWall){ 

  enum botType{Guard, Watcher};
  imageWall = cv::Mat::zeros(12*ObjectData::sizeCase, 12*ObjectData::sizeCase, CV_8UC3);
  c3ga::Mvec<double> sphere = c3ga::point<double>(1,0.5,0)
                        ^ c3ga::point<double>(0.5,1,0)
                        ^ c3ga::point<double>(1.5,1,0)
                        ^ c3ga::point<double>(1,1,0.5);   

  c3ga::Mvec<double> t = c3ga::vector<double>(0,10,0);
  c3ga::Mvec<double> translator = 1-0.5*t*c3ga::ei<double>();
  auto spherePlayer = (translator*sphere/translator);
  player.ObjectDynamique(spherePlayer,0,0,200);

  //Win
  t = c3ga::vector<double>(8,2,0);
  translator = 1-0.5*t*c3ga::ei<double>();
  auto sphereWin = (translator*sphere/translator);
  win.ObjectDynamique(sphereWin,0,100,0);

  //Wall
  auto walls = GenerateWall2();
  for (int i =0; i<(int)walls.size(); i++){
    std::shared_ptr<Object> wallC3GA = std::shared_ptr<Object>(new Object());
    wallC3GA->ObjectStatic(walls[i]);
    lstWall.push_back(wallC3GA);
    wallC3GA->draw(imageWall);
  }

  //NPC
  //Positionnement bot
  typeBot.push_back(botType::Watcher);
  lstNpc.push_back(std::shared_ptr<Object> (new Object()));
  t = c3ga::vector<double>(4,6,0);
  translator = 1-0.5*t*c3ga::ei<double>();
  auto sphereNPC = (translator*sphere/translator);
  lstNpc[0]->ObjectDynamique(sphereNPC,100,50,0);
  lstTree.push_back(std::shared_ptr<RechercheTree>(new RechercheTree(walls ,lstNpc[0]->getObj())));

}

/******* Map1 *******/



std::vector<c3ga::Mvec<double>> GenerateWall1(){
  // border
  std::vector<c3ga::Mvec<double>> vec;
  c3ga::Mvec<double> wall = c3ga::point<double>(0,0,0)
                          ^ c3ga::point<double>(0,10,0)
                          ^ c3ga::point<double>(0,5,5);
  vec.push_back(wall);
  
  wall = c3ga::point<double>(0,0,0)
        ^ c3ga::point<double>(10,0,0)
        ^ c3ga::point<double>(5,0,5);
  vec.push_back(wall);

  wall = c3ga::point<double>(0,10,0)
        ^ c3ga::point<double>(10,10,0)
        ^ c3ga::point<double>(5,10,5);
  vec.push_back(wall);

  wall = c3ga::point<double>(10,0,0)
        ^ c3ga::point<double>(10,10,0)
        ^ c3ga::point<double>(10,5,5);
  vec.push_back(wall);
  // in wall
  //1
  wall = c3ga::point<double>(2,2,0)
        ^ c3ga::point<double>(6,2,0)
        ^ c3ga::point<double>(4,2,2);
  vec.push_back(wall);

  wall = c3ga::point<double>(2,2,0)
        ^ c3ga::point<double>(2,4,0)
        ^ c3ga::point<double>(2,3,1);
  vec.push_back(wall);
  //2
  wall = c3ga::point<double>(2,6,0)
        ^ c3ga::point<double>(2,8,0)
        ^ c3ga::point<double>(2,7,1);
  vec.push_back(wall);
  //3
  wall = c3ga::point<double>(4,4,0)
        ^ c3ga::point<double>(4,8,0)
        ^ c3ga::point<double>(4,6,2);
  vec.push_back(wall);
  
  wall = c3ga::point<double>(4,4,0)
        ^ c3ga::point<double>(6,4,0)
        ^ c3ga::point<double>(5,4,1);
  vec.push_back(wall);
  //4
  wall = c3ga::point<double>(8,2,0)
        ^ c3ga::point<double>(8,8,0)
        ^ c3ga::point<double>(8,5,3);
  vec.push_back(wall);
  
  wall = c3ga::point<double>(6,8,0)
        ^ c3ga::point<double>(8,8,0)
        ^ c3ga::point<double>(7,8,1);

  wall = c3ga::point<double>(6,6,0)
        ^ c3ga::point<double>(6,10,0)
        ^ c3ga::point<double>(6,9,2);

  vec.push_back(wall);
  return vec;
}

void buildMap1(Object &player, 
  Object &win, 
  std::vector<std::shared_ptr<Object>> &lstWall,
  std::vector<std::shared_ptr<Object>> &lstNpc,
  std::vector<std::shared_ptr<RechercheTree>> &lstTree,
  std::vector<int> &typeBot,
  cv::Mat &imageWall){ 
  imageWall = cv::Mat::zeros(10*ObjectData::sizeCase, 10*ObjectData::sizeCase, CV_8UC3);
  c3ga::Mvec<double> sphere = c3ga::point<double>(1,0.5,0)
                        ^ c3ga::point<double>(0.5,1,0)
                        ^ c3ga::point<double>(1.5,1,0)
                        ^ c3ga::point<double>(1,1,0.5);
  player.ObjectDynamique(sphere,0,0,200);
  c3ga::Mvec<double> t = 2*(c3ga::e1<double>());
  c3ga::Mvec<double> translator = 1-0.5*t*c3ga::ei<double>();
  auto sphereWin = (translator*sphere/translator);
  t = 2*(c3ga::e2<double>());
  translator = 1-0.5*t*c3ga::ei<double>();
  enum botType{Guard, Watcher};
  //Win
  sphereWin = (translator*sphereWin/translator);
  win.ObjectDynamique(sphereWin,0,100,0);

  //Wall
  auto walls = GenerateWall1();
  for (int i =0; i<(int)walls.size(); i++){
    std::shared_ptr<Object> wallC3GA = std::shared_ptr<Object>(new Object());
    wallC3GA->ObjectStatic(walls[i]);
    lstWall.push_back(wallC3GA);
    wallC3GA->draw(imageWall);
  }

  //NPC
  //Positionnement bot
  typeBot.push_back(botType::Guard);
  lstNpc.push_back(std::shared_ptr<Object> (new Object()));
  t = 8*(c3ga::e1<double>());
  translator = 1-0.5*t*c3ga::ei<double>();
  auto sphereNPC = (translator*sphere/translator);
  t = 8*(c3ga::e2<double>());
  translator = 1-0.5*t*c3ga::ei<double>();
  sphereNPC = (translator*sphereNPC/translator);
  lstNpc[0]->ObjectDynamique(sphereNPC,100,0,0);
  lstTree.push_back(std::shared_ptr<RechercheTree>(new RechercheTree(walls ,lstNpc[0]->getObj())));

}


