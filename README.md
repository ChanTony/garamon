# GameC3GA

## Jeux
Objectif :
vous devez rejoindre la sortie pour gagner

Element du plateau :
Vous étes le cercle bleu
Les Bots sont les cercles rouge et orange
Le cercle vert est la sortie
Les éléments gris sont les murs

Nombre de niveau : 3

## Librairie 
 - Openmp
 - Opencv 3.1
 - C3GA

## Installation  
 1. cd garamon
 2. mkbir build
 3. cd build
 4. cmake ../
 5. make
 6. ./GameC3GA

## Commande de jeux
<p>Le click gauche de la souris indique où vous allez.</p>
<p>Il est indiqué par une croix bleu sur la fenêtre.</p>
<p>Vous ne passez pas à travers les murs.</p>